﻿using System;

namespace MindWorX.Blizzard.Storm
{
    public class Archive : IDisposable
    {
        public Archive(IntPtr handle)
        {
            this.Handle = handle;
        }

        public IntPtr Handle { get; }

        public void Dispose()
        {
            if (this.Handle == IntPtr.Zero)
                return;
            SFile.CloseArchive(this.Handle);
        }
    }
}
