﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.Blizzard.Storm
{
    public unsafe class SMem
    {
        public const string LIBRARY_NAME = "Storm.dll";

        public static IntPtr Handle
        {
            get
            {
                var handle = Kernel32.GetModuleHandle(LIBRARY_NAME);
                return handle != IntPtr.Zero ? handle : Kernel32.LoadLibrary(LIBRARY_NAME);
            }
        }

        public static IntPtr InitPtr => Kernel32.GetProcAddress(Handle, 400);

        public static IntPtr AllocPtr => Kernel32.GetProcAddress(Handle, 401);

        public static IntPtr DestroyPtr => Kernel32.GetProcAddress(Handle, 402);

        public static IntPtr FreePtr => Kernel32.GetProcAddress(Handle, 403);

        public static IntPtr GetSizePtr => Kernel32.GetProcAddress(Handle, 404);

        public static IntPtr ReAllocPtr => Kernel32.GetProcAddress(Handle, 405);


        [UnmanagedFunctionPointer(CallingConvention.Winapi)]
        public delegate void InitPrototype();

        [UnmanagedFunctionPointer(CallingConvention.Winapi)]
        public delegate IntPtr AllocPrototype([In] int amount, [In] char* filename, [In] int logline, [In] int defaultValue);

        [UnmanagedFunctionPointer(CallingConvention.Winapi)]
        public delegate void DestroyPrototype();

        [UnmanagedFunctionPointer(CallingConvention.Winapi)]
        public delegate bool FreePrototype([In] IntPtr location, [In] char* filename, [In] int logline, [In] int defaultValue);

        [UnmanagedFunctionPointer(CallingConvention.Winapi)]
        public delegate int GetSizePrototype([In] IntPtr location, [In] char* filename, [In] int logline);

        [UnmanagedFunctionPointer(CallingConvention.Winapi)]
        public delegate IntPtr ReAllocPrototype([In] IntPtr location, [In] int amount, [In] char* filename, [In] int logline, [In] int defaultValue);


        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#400")]
        public static extern void Init();


        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#401")]
        public static extern IntPtr Alloc([In] int amount, [In] char* filename, [In] int logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#401")]
        public static extern IntPtr Alloc([In] int amount, [In] string filename, [In] int logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#401")]
        public static extern IntPtr Alloc([In] int amount, [In] char* filename, [In] LogLine logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#401")]
        public static extern IntPtr Alloc([In] int amount, [In] string filename, [In] LogLine logline, [In] int defaultValue);

        public static IntPtr Alloc(int amount, int defaultValue = 0)
        {
            var stackframe = new StackFrame(1, true);
            return Alloc(amount, stackframe.GetFileName(), stackframe.GetFileLineNumber(), defaultValue);
        }


        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#402")]
        public static extern void Destroy();


        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#403")]
        public static extern bool Free([In] IntPtr location, [In] char* filename, [In] int logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#403")]
        public static extern bool Free([In] IntPtr location, [In] string filename, [In] int logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#403")]
        public static extern bool Free([In] IntPtr location, [In] char* filename, [In] LogLine logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#403")]
        public static extern bool Free([In] IntPtr location, [In] string filename, [In] LogLine logline, [In] int defaultValue);

        public static bool Free(IntPtr location, int defaultValue = 0)
        {
            var stackframe = new StackFrame(1, true);
            return Free(location, stackframe.GetFileName(), stackframe.GetFileLineNumber(), defaultValue);
        }


        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#404")]
        public static extern int GetSize([In] IntPtr location, [In] char* filename, [In] int logline);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#404")]
        public static extern int GetSize([In] IntPtr location, [In] string filename, [In] int logline);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#404")]
        public static extern int GetSize([In] IntPtr location, [In] char* filename, [In] LogLine logline);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#404")]
        public static extern int GetSize([In] IntPtr location, [In] string filename, [In] LogLine logline);

        public static int GetSize(IntPtr location)
        {
            var stackframe = new StackFrame(1, true);
            return GetSize(location, stackframe.GetFileName(), stackframe.GetFileLineNumber());
        }


        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#405")]
        public static extern IntPtr ReAlloc([In] IntPtr location, [In] int amount, [In] char* filename, [In] int logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#405")]
        public static extern IntPtr ReAlloc([In] IntPtr location, [In] int amount, [In] string filename, [In] int logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#405")]
        public static extern IntPtr ReAlloc([In] IntPtr location, [In] int amount, [In] char* filename, [In] LogLine logline, [In] int defaultValue);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Winapi, EntryPoint = "#405")]
        public static extern IntPtr ReAlloc([In] IntPtr location, [In] int amount, [In] string filename, [In] LogLine logline, [In] int defaultValue);

        public static IntPtr ReAlloc(IntPtr location, int amount, int defaultValue = 0)
        {
            var stackframe = new StackFrame(1, true);
            return ReAlloc(location, amount, stackframe.GetFileName(), stackframe.GetFileLineNumber(), defaultValue);
        }


        private SMem() { }
    }

    public enum LogLine
    {
        Expression = 0,
        Function = -1,
        Object = -2,
        Handle = -3,
        File = -4
    }
}
