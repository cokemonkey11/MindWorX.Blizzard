﻿using System;
using System.Text;

namespace MindWorX.Blizzard
{
    /// <summary>
    /// A little-endian encoded ObjectId.
    /// </summary>
    [Serializable]
    public struct ObjectIdL
    {
        public static ObjectIdL Parse(string id)
        {
            if (id.Length != 4)
                throw new FormatException("Invalid length. Must be 4 characters long.");
            try
            {
                var bytes = Encoding.ASCII.GetBytes(id);
                return new ObjectIdL(bytes[3] | (bytes[2] << 8) | (bytes[1] << 16) | (bytes[0] << 24));
            }
            catch (Exception e)
            {
                throw new FormatException("Parsing failed. See inner exception for details.", e);
            }
        }

        public static bool TryParse(string id, out ObjectIdL objectId)
        {
            objectId = default(ObjectIdL);
            if (id.Length != 4)
                return false;
            try
            {
                var bytes = Encoding.ASCII.GetBytes(id);
                objectId = new ObjectIdL(bytes[3] | (bytes[2] << 8) | (bytes[1] << 16) | (bytes[0] << 24));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private readonly int value;

        public ObjectIdL(int id)
        {
            this.value = id;
        }

        public ObjectIdL(string id)
        {
            if (id.Length != 4)
                throw new ArgumentOutOfRangeException(nameof(id), "Invalid id. Must be 4 characters long");

            this.value = (id[3]) | (id[2] << 8) | (id[1] << 16) | (id[0] << 24);
        }

        // Explicit conversion from ObjectIdL to Int32
        public static explicit operator int(ObjectIdL from)
        {
            return from.value;
        }

        // Explicit conversion from Int32 to ObjectIdL
        public static explicit operator ObjectIdL(int from)
        {
            return new ObjectIdL(from);
        }

        // Explicit conversion from ObjectIdB to ObjectIdL
        public static explicit operator ObjectIdL(ObjectIdB from)
        {
            var bytes = BitConverter.GetBytes((int)from);
            Array.Reverse(bytes);
            return new ObjectIdL(BitConverter.ToInt32(bytes, 0));
        }

        public override string ToString()
        {
            var bytes = BitConverter.GetBytes(this.value);
            Array.Reverse(bytes);
            return Encoding.ASCII.GetString(bytes);
        }
    }
}
